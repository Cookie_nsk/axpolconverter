﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxpolConverter
{
    public class ThreadParams
    {
        public string FileName { get; set; }
        public string SchemaFileName { get; set; }
        public string LoadDataType { get; set; }
        public object Control { get; set; }
        public string MainDataFileName { get; set; }
        public string PrintDataFileName { get; set; }
        public object SettingsData { get; set; }
        public string OutputFileName { get; set; }
        public ConvertItemsContainer ExportData { get; set; }
        public DateTime StartedAt { get; set; }
        public object ProgressBar { get; set; }
        public object MainForm { get; set; }
        public Exception Exception { get; set; }
        public  Dictionary<string, double> HandlingCost { get; set; }
        public  Dictionary<string, double> PreparationCost { get; set; }
        public  Dictionary<int, Dictionary<string, double>> PrintMethodCost { get; set; }
        public List<TemplateColumnReference> PricesColumnReferences { get; set; }
        public List<TemplateColumnReference> DataColumnReferences { get; set; }
    }
}
