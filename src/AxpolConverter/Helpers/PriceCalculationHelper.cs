﻿using AxpolConverter.Core.Domain;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AxpolConverter
{
    public static class PriceCalculationHelper
    {
        private static Dictionary<string, double> HandlingCost { get; set; }
        private static Dictionary<string, double> PreparationCost { get; set; }
        private static Dictionary<int, Dictionary<string, double>> PrintMethodCost { get; set; }

        public static string CalculateBasicItemPriceAndAdditionalAttributes(AxpolItem item, double itemsQuantity, 
            double discount, double excangeRate, double numberOfColors, ThreadParams prms)
        {//((basePrice – (basePrice * tradeDiscount)) * items_quantity) + (T3 * items_quantity) + (handling_cost * items_quantity) + (Preparation Cost P1 * NumberOfColours)
            try
            {
                CultureInfo cult = new CultureInfo("en-GB");
                double basePrice = 0;

                double printMethodCost = prms.PrintMethodCost[int.Parse(itemsQuantity.ToString())].First(x=>x.Key.Equals(item.CurrentPrintMethod)).Value;
                double handlingCost = !string.IsNullOrEmpty(item.RowData.HandlingCost) ? prms.HandlingCost[item.RowData.HandlingCost] : 0;
                double preparationCost = prms.PreparationCost[item.CurrentPrintMethod];

                if (double.TryParse(item.RowData.NetPriceEUR, NumberStyles.Any, cult, out basePrice))
                {
                    double totalBasePrice = ((basePrice - (basePrice * (discount / 100))) * itemsQuantity) +
                        (printMethodCost * itemsQuantity) +
                        (handlingCost * itemsQuantity) +
                        (preparationCost * numberOfColors);

                    double totalGBPPerItem = Math.Round((totalBasePrice * excangeRate) / itemsQuantity, 2);

                    return string.Format("{0}", totalGBPPerItem.ToString("F"));
                }
            }
            catch (Exception exp)
            {
                NLogHelper.Error(exp);

            }
            return "";
        }
    }
}
