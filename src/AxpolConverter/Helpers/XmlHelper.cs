﻿using AxpolConverter.Core.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace AxpolConverter
{
    public static class XmlHelper
    {
        public static ConvertItemsContainer CreateItemsForConvertion(object thrdParams)
        {
            ThreadParams threadParams = (ThreadParams)thrdParams;

            List<RowData> rawDataList = null;
            List<RowPrint> printDataList = null;

            using (FileStream fileStream = new FileStream(threadParams.MainDataFileName, FileMode.Open))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<RowData>),
                new XmlRootAttribute("Root"));
                rawDataList = (List<RowData>)xmlSerializer.Deserialize(fileStream);
            }

            using (FileStream fileStream = new FileStream(threadParams.PrintDataFileName, FileMode.Open))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<RowPrint>),
                new XmlRootAttribute("Root"));
                printDataList = (List<RowPrint>)xmlSerializer.Deserialize(fileStream);
            }

            ConvertItemsContainer retItem = CreateAndValidateInputXmlData(rawDataList, printDataList, threadParams);
            retItem.FileName = threadParams.OutputFileName;

            return retItem;
        }
        public static ConvertItemsContainer CreateAndValidateInputXmlData(List<RowData> rawData, List<RowPrint> printData, ThreadParams threadParams)
        {
            ConvertItemsContainer retItem = new ConvertItemsContainer();
            ProgressBar prgrsBar = (threadParams.ProgressBar as ProgressBar);

            SetUpProgressBar(rawData.Count, prgrsBar);

            SettingsDto settings = (threadParams.SettingsData as SettingsDto);

            foreach (RowData item in rawData)
            {
                AxpolItem axItem = new AxpolItem();

                RowPrint printItem = printData.FirstOrDefault(x => x.CodeERP.ToLower().Equals(item.CodeERP.ToLower()));

                if (printItem != null && CreateAxpolItem(axItem, item, printItem, settings))
                {
                    retItem.ValidItems.Add(axItem);
                }
                else
                {
                    if (item.Errors == null)
                    {
                        item.Errors = new List<string>();
                    }
                    item.Errors.Add("No print data available in print-XML.");
                    retItem.InvalidItems.Add(item);
                }

                MethodInvoker m = new MethodInvoker(() => prgrsBar.PerformStep());
                prgrsBar.Invoke(m);
            }

            EnumerateAxpolItems(retItem.ValidItems);

            return retItem;
        }
        private static bool CreateAxpolItem(AxpolItem item, RowData rowData, RowPrint printData, SettingsDto settings)
        {
            item.PrintData = printData;
            item.RowData = rowData;

            rowData.PrintData = printData;

            if (!string.IsNullOrEmpty(rowData.TitleEN))
            {
                item.Title = rowData.TitleEN;
            }
            else
            {
                rowData.Errors.Add("TitleEN is empty");
            }

            if (!string.IsNullOrEmpty(rowData.DescriptionEN))
            {
                item.Description = rowData.DescriptionEN;
            }
            else
            {
                rowData.Errors.Add("DescriptionEN is empty");
            }

            item.Supplier = settings.Supplier;
            item.SupplierCode = settings.SupplierCode;

            if (!string.IsNullOrEmpty(rowData.CodeERP))
            {
                item.SupplierReference = rowData.CodeERP;
            }
            else
            {
                rowData.Errors.Add("CodeERP is empty");
            }

            try
            {
                ParseImages(item, rowData);
            }
            catch
            {
                rowData.Errors.Add("Error wile parsing images block from Foto01 to Foto20");
            }

            item.Colour = rowData.ColorEN;

            if (!string.IsNullOrEmpty(rowData.Dimensions))
            {
                item.Size = rowData.Dimensions.Replace("&#216;", string.Empty);
            }
            else
            {
                rowData.Errors.Add("Dimensions is empty");
            }

            try
            {
                FillPrintFieldsData(item, printData);
            }
            catch
            {
                rowData.Errors.Add("Error wile parsing print method blocks");
            }

            item.MOQ = settings.MOQ;

            if (!string.IsNullOrEmpty(rowData.MaterialEN))
            {
                item.Material = rowData.MaterialEN;
            }
            else
            {
                rowData.Errors.Add("MaterialEN is empty");
            }

            item.AdditionalCharges = settings.AdditionalCharges;
            item.CofO = rowData.CountryOfOrigin;

            return rowData.Errors.Count == 0;
        }
        public static List<string> ValidateXmlByXsdSchema(string fileName, string xsdScemaName)
        {
            List<string> errorsList = new List<string>();

            using (TextReader reader = File.OpenText(string.Format("./Xsd/{0}", xsdScemaName)))
            {
                try
                {
                    XmlSchemaSet schema = new XmlSchemaSet();
                    schema.Add("", XmlReader.Create(reader));

                    XDocument xmlDoc = XDocument.Load(fileName);

                    xmlDoc.Validate(schema, (o, e) =>
                    {
                        errorsList.Add(e.Message);
                    });
                }
                catch (Exception exp)
                {
                    NLogHelper.Error(exp);
                }
            }

            return errorsList;
        }
        private static void EnumerateAxpolItems(List<AxpolItem> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].SKUNumber = string.Format("ITB{0}", 1000 + i);
            }
        }

        #region Print fields data
        private static void FillPrintFieldsData(AxpolItem axpolItem, RowPrint printData)
        {
            FillPrintMethodsPrintColorFields(axpolItem, printData);
            try
            {
                FillPrintOptionDescriptionAndPosition(axpolItem, printData);
            }
            catch (Exception e)
            {
                NLogHelper.Error(e);
            }

        }
        /// <summary>
        /// Here field currentPrintMethod is initialized. This field is used for further price calculation.
        /// </summary>
        /// <param name="axpolItem"></param>
        /// <param name="printData"></param>
        private static void FillPrintMethodsPrintColorFields(AxpolItem axpolItem, RowPrint printData)
        {
            List<string> printMethods = new List<string>();
            List<string> printColors = new List<string>();
            List<string> printMethodsShort = new List<string>();

            for (int i = 1; i <= 6; i++)
            {
                for (int j = 1; j <= 5; j++)
                {
                    string value = PropertyValuesHelper.GetPropValue(printData,
                        string.Format("Position_{0}_PrintTech_{1}", i, j)).ToString();
                    if (!string.IsNullOrEmpty(value))
                    {
                        printMethodsShort.Add(value);
                        printMethods.Add(GetPrintPositionFullName(value));
                        printColors.Add(GetPrintColorTypeName(value));
                    }
                }
            }

            if (printMethods.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string item in printMethods.Distinct())
                {
                    sb.AppendFormat("{0},", item);
                }

                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }

                axpolItem.PrintMethods = printMethodsShort.Distinct().ToList();
                axpolItem.PrintMethod = sb.ToString();
            }

            if (printColors.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string item in printColors.Distinct())
                {
                    sb.AppendFormat("{0},", item);
                }

                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }

                axpolItem.PrintColours = sb.ToString();
            }
        }
        private static void FillPrintOptionDescriptionAndPosition(AxpolItem axpolItem, RowPrint printData)
        {
            //"casing front, 25x60mm, 1 colour pad print
            //casing front, 25x60mm, full colour print "
            List<string> printPosition = new List<string>();

            axpolItem.PrintingPricingParams = new List<PrintingPricingParams>();

            for (int i = 1; i <= 6; i++)
            {//i <= 6 - total count of items int print_data.xml
                List<string> printTechShort = new List<string>();

                string printPositionStr = PropertyValuesHelper.GetPropValue(printData,
                        string.Format("Position_{0}_PrintPosition", i)).ToString();

                string printSizeStr = PropertyValuesHelper.GetPropValue(printData,
                        string.Format("Position_{0}_PrintSize", i)).ToString();

                for (int j = 1; j <= 5; j++)
                {
                    string value = PropertyValuesHelper.GetPropValue(printData,
                        string.Format("Position_{0}_PrintTech_{1}", i, j)).ToString();
                    if (!string.IsNullOrEmpty(value))
                    {
                        printTechShort.Add(value);
                    }
                }

                if (!string.IsNullOrEmpty(printPositionStr) &&
                   !string.IsNullOrEmpty(printSizeStr) &&
                   printTechShort.Count > 0)
                {
                    foreach (string item in printTechShort.Distinct())
                    {
                        if (!string.IsNullOrEmpty(GetPrintDescriptionColorTypeName(item)))
                        {
                            PrintingPricingParams prntPrms = new PrintingPricingParams();

                            prntPrms.PrintingOptionDescription = string.Format("{0}, {1}, {2}",
                            GetPrintDescriptionColorTypeName(item),
                            printSizeStr,
                            printPositionStr);

                            prntPrms.PrintingMethod = item.ToUpper();
                            axpolItem.PrintingPricingParams.Add(prntPrms);

                            axpolItem.PrintPositon = string.Concat(axpolItem.PrintPositon, 
                                string.Format("{0}, {1} mm, {2}\r\n",
                            printPositionStr,
                            printSizeStr,
                            GetPrintDescriptionColorTypeName(item)
                            ));
                        }
                    }
                }
            }
        }
        private static string GetPrintPositionFullName(string shortName)
        {
            switch (shortName.Trim().ToLower())
            {
                case "t1":
                case "t2":
                case "t3":
                case "t4":
                    return "Pad Print";

                case "s1":
                case "s2":
                case "s3":
                case "s4":
                    return "Screenprint";

                case "tf1":
                case "tf2":
                case "tf3":
                case "tf4":
                case "kd7":
                    return "Heat Transfer";

                case "tc1":
                case "tc2":
                    return "Hot Stamp/Emboss";

                case "l0":
                case "l1":
                case "l2":
                case "l3":
                case "l4":
                    return "Laser Engraving";

                case "fc1":
                case "fc2":
                    return "Full Colour Print";

                case "sl":
                    return "Dye Sublimation";

                default:
                    return string.Empty;
            }
        }
        private static string GetPrintColorTypeName(string shortName)
        {
            switch (shortName.Trim().ToLower())
            {
                case "t1":
                case "t2":
                case "t3":
                case "t4":
                case "s1":
                case "s2":
                case "s3":
                case "s4":
                case "tf1":
                case "tf2":
                case "tf3":
                    return "1 Colour";

                case "tc1":
                case "tc2":
                    return "Embossing or 1 Colour Hot Stamp";

                case "l0":
                case "l1":
                case "l2":
                case "l3":
                case "l4":
                    return "Laser Engraving";

                case "fc1":
                case "fc2":
                case "sl":
                    return "Full Colour";

                default:
                    return string.Empty;
            }
        }
        private static string GetPrintDescriptionColorTypeName(string shortName)
        {
            switch (shortName.Trim().ToLower())
            {
                case "t1":
                case "t2":
                case "t3":
                case "t4":
                    return "1 Colour Pad Print";

                case "s1":
                case "s2":
                case "s3":
                case "s4":
                    return "1 Colour Screenprint";

                case "tf1":
                case "tf2":
                case "tf3":
                case "tf4":
                case "kd7":
                    return "1 Colour Transfer";

                case "tc1":
                case "tc2":
                    return "Embossing or 1 Colour Hot Stamp";

                case "l0":
                case "l1":
                case "l2":
                case "l3":
                case "l4":
                    return "Laser Engraving";

                case "fc1":
                case "fc2":
                    return "Full Colour Print";

                case "sl":
                    return "Full Colour Sublimation";

                default:
                    return string.Empty;
            }
        }
        #endregion

        private static void ParseImages(AxpolItem item, RowData rawData)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 1; i <= 20; i++)
            {//Getting properties from Foto01 to Foto20

                string value = PropertyValuesHelper
                    .GetPropValue<string>(rawData, string.Format("Foto{0}", i < 10 ? string.Format("0{0}", i) : i.ToString()));

                if (!string.IsNullOrEmpty(value))
                {
                    sb.AppendFormat("{0},", value);
                }
            }

            if (sb.Length > 0)
            {
                item.Images = sb.ToString().Substring(0, sb.ToString().Length - 1);
            }
        }
        private static void SetUpProgressBar(int totalValues, ProgressBar prgrsBar)
        {
            prgrsBar.Invoke((MethodInvoker)delegate
            {
                prgrsBar.Minimum = 1;
                prgrsBar.Maximum = totalValues;
                prgrsBar.Value = 1;
                prgrsBar.Step = 1;
            });
        }
    }
}
