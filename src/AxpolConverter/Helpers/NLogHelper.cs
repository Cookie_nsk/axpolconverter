﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using NLog;

namespace AxpolConverter
{
    public static class NLogHelper
    {
        public static void Error(Exception exp)
        {
            LogManager.GetCurrentClassLogger().Error(exp);
        }
        
    }
}
