﻿using AxpolConverter.Core.Domain;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AxpolConverter
{
    public static class XlsxHelper
    {
        public static ThreadParams CreateXlsxFileEntries(ThreadParams parameters)
        {
            string outputFileName = parameters.OutputFileName;

            List<Tuple<string, string>> colNames = GetColumnNamesFromAppSettings(parameters.DataColumnReferences);
            try
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(outputFileName, true))
                {
                    WorkbookPart wbPart = spreadsheetDocument.WorkbookPart;
                    Sheet sheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Customer Price List").FirstOrDefault();
                    WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(sheet.Id));
                    InsertItemsToXSLX(parameters.ExportData.ValidItems,
                        spreadsheetDocument,
                        wsPart, colNames, (parameters.ProgressBar as ProgressBar), (parameters.SettingsData as SettingsDto), parameters);
                }
            }
            catch (Exception exp)
            {
                parameters.Exception = exp;
                NLogHelper.Error(exp);
            }

            return parameters;
        }
        public static void InsertItemsToXSLX(List<AxpolItem> items, SpreadsheetDocument sDoc,
            WorksheetPart wsPart, List<Tuple<string, string>> colNames, ProgressBar prgrsBar,
            SettingsDto dto, ThreadParams prms)
        {
            int rowIndex = 2;

            foreach (AxpolItem item in items)
            {
                try
                {
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.ITBSKUCode.ToString(), colNames), item.SKUNumber);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Title.ToString(), colNames), item.Title);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Description.ToString(), colNames), item.Description);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Supplier.ToString(), colNames), item.Supplier);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.SupplierCode.ToString(), colNames), item.SupplierCode);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.SupplierReference.ToString(), colNames), item.SupplierReference);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Images.ToString(), colNames), item.Images);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.SupplierCategory.ToString(), colNames), item.SupplierCategory);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Attribute1_Colour.ToString(), colNames), item.Colour);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Attribute2_Size.ToString(), colNames), item.Size);

                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Attribute3_PrintMethod.ToString(), colNames), item.PrintMethod);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Attribute4_PrintPositon.ToString(), colNames), item.PrintPositon);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Attribute5_PrintArea.ToString(), colNames), item.PrintArea);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Attribute6_PrintColours.ToString(), colNames), item.PrintColours);

                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Attribute7_MOQ.ToString(), colNames), item.MOQ);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Attribute8_Material.ToString(), colNames), item.Material);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Attribute9_AdditionalCharges.ToString(), colNames), item.AdditionalCharges);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.Attribute10_LeadTime.ToString(), colNames), item.LeadTime);

                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.CofO.ToString(), colNames), item.CofO);
                    InsertText(sDoc, wsPart, rowIndex, GetColumnName(XslxColumNames.SupplierPriceExpiry.ToString(), colNames), item.SupplierPriceExpiry);

                    try
                    {//Pricing output
                        CultureInfo cult = new CultureInfo("en-GB");
                        #region Prices

                        int priceSectionCount = item.PrintingPricingParams.Count > prms.PricesColumnReferences.Count ?
                            prms.PricesColumnReferences.Count :
                            item.PrintingPricingParams.Count;

                        int selectionIndex = 0;

                        foreach (PrintingPricingParams prntPrcParam in item.PrintingPricingParams)
                        {
                            if (selectionIndex >= priceSectionCount)
                            {
                                continue;
                            }
                            //Inserting print item description
                            InsertText(sDoc, wsPart, rowIndex, prms.PricesColumnReferences[selectionIndex].ColumnReference, prntPrcParam.PrintingOptionDescription);

                            foreach (TemplateColumnReference refernc in prms.PricesColumnReferences[selectionIndex].ChildColumns)
                            {//Inserting prices for each cost (50,100,250...5000)
                             //refernc.ColumnType - contains Price50,Price100...
                                int itemsQuantity = int.Parse(refernc.ColumnType.ToString().Replace("Price", ""));

                                item.CurrentPrintMethod = prntPrcParam.PrintingMethod;

                                InsertText(sDoc, wsPart, rowIndex,
                                    refernc.ColumnReference,
                                    PriceCalculationHelper.CalculateBasicItemPriceAndAdditionalAttributes(item, itemsQuantity, int.Parse(dto.AxplDiscount),
                                    double.Parse(dto.ConvertionRate, cult), 1, prms),
                                    true);
                            }

                            selectionIndex++;
                        }
                    }
                    catch (Exception e)
                    {
                        NLogHelper.Error(e);
                    }
                    #endregion
                    rowIndex++;

                    MethodInvoker m = new MethodInvoker(() => prgrsBar.PerformStep());
                    prgrsBar.Invoke(m);
                }
                catch (Exception exp)
                {
                    NLogHelper.Error(exp);
                }
            }

            wsPart.Worksheet.Save();
        }
        private static bool CheckIfItemHasAPrintMethod(XslxColumNames printMethodName, AxpolItem item)
        {
            if (item.PrintMethods != null && item.PrintMethods.Count > 0)
            {
                if (printMethodName == XslxColumNames.OneColourPadPrint)
                {
                    List<string> compList = new List<string>() { "T1", "T2", "T3", "T4" };
                    return FindPringMethodInItem(item, compList);
                }
                else if (printMethodName == XslxColumNames.OneColourScreenPrint)
                {
                    List<string> compList = new List<string>() { "S1", "S2", "S3", "S4" };
                    return FindPringMethodInItem(item, compList);
                }
                else if (printMethodName == XslxColumNames.OneColourTransferPrint)
                {
                    List<string> compList = new List<string>() { "TF1", "TF2", "TF3" };
                    return FindPringMethodInItem(item, compList);
                }
                else if (printMethodName == XslxColumNames.EmbossingOrOneColourHotStamp)
                {
                    List<string> compList = new List<string>() { "TC1", "TC2" };
                    return FindPringMethodInItem(item, compList);
                }
                else if (printMethodName == XslxColumNames.LaserEngraving)
                {
                    List<string> compList = new List<string>() { "L0", "L1", "L2", "L3", "L4" };
                    return FindPringMethodInItem(item, compList);
                }
                else if (printMethodName == XslxColumNames.FullColourPrint)
                {
                    List<string> compList = new List<string>() { "FC1", "FC2" };
                    return FindPringMethodInItem(item, compList);
                }
                else if (printMethodName == XslxColumNames.FullColourSublimation)
                {
                    List<string> compList = new List<string>() { "SL" };
                    return FindPringMethodInItem(item, compList);
                }
            }

            return false;
        }
        private static bool FindPringMethodInItem(AxpolItem item, List<string> compList)
        {
            if (item.PrintMethods.Count(y => compList.Contains(y.ToString())) > 0)
            {
                item.CurrentPrintMethod = item.PrintMethods.First(x => compList.Contains(x));
                return true;
            }
            return false;
        }
        private static List<Tuple<string, string>> GetColumnNamesFromAppSettings(List<TemplateColumnReference> inputData)
        {
            List<Tuple<string, string>> list = new List<Tuple<string, string>>();

            foreach (TemplateColumnReference item in inputData)
            {
                list.Add(new Tuple<string, string>(item.ColumnType.ToString(), item.ColumnReference));
            }
            return list;
        }
        private static string GetColumnName(string colName, List<Tuple<string, string>> tuples)
        {
            return tuples.First(x => x.Item1.Equals(colName)).Item2;
        }
        public static object CreateErrorXlsl(ConvertItemsContainer items)
        {
            string outputFileName = string.Format("./ExportResults/{0}", items.FileName);

            try
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(outputFileName, true))
                {
                    WorkbookPart wbPart = spreadsheetDocument.WorkbookPart;
                    Sheet sheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Invalid items").FirstOrDefault();
                    WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(sheet.Id));

                    InsertInvalidItems(items.InvalidItems, spreadsheetDocument, wsPart);
                }
            }
            catch (Exception exp)
            {
                NLogHelper.Error(exp);
            }

            return items.FileName;
        }
        public static void InsertInvalidItems(List<RowData> invalidItems, SpreadsheetDocument spreadSheet, WorksheetPart wsPart)
        {
            int rowIndex = 2;
            foreach (RowData item in invalidItems)
            {
                try
                {
                    InsertText(spreadSheet, wsPart, rowIndex, "A", item.CodeERP);
                    InsertText(spreadSheet, wsPart, rowIndex, "B", item.TitleEN);
                    StringBuilder sb = new StringBuilder();
                    foreach (string err in item.Errors)
                    {
                        sb.AppendFormat("{0}\r\n", err);
                    }
                    InsertText(spreadSheet, wsPart, rowIndex, "C", sb.ToString());
                    rowIndex++;
                }
                catch (Exception e)
                {
                    NLogHelper.Error(e);
                }
            }

            wsPart.Worksheet.Save();
        }

        #region OpenXml 2.5 methods
        public static void InsertText(SpreadsheetDocument spreadSheet, WorksheetPart wsPart, int rowIndex, string cellName, string text, bool isNumber = false)
        {
            // Get the SharedStringTablePart. If it does not exist, create a new one.
            SharedStringTablePart shareStringPart =
                spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();

            // Insert cell into the new worksheet.
            Cell cell = InsertCellInWorksheet(cellName, (uint)rowIndex, wsPart);

            cell.DataType = new EnumValue<CellValues>(CellValues.String);
            cell.CellValue = new CellValue(text);

        }
        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (cell.CellReference.Value.Length == cellReference.Length)
                    {
                        if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                        {
                            refCell = cell;
                            break;
                        }
                    }
                }
                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                return newCell;
            }
        }
        #endregion

    }
}
