﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AxpolConverter
{
    public enum XslxColumNames
    {
        ITBSKUCode = 0,
        Title,
        Description,
        Supplier,
        SupplierCode,
        SupplierReference,
        Images,
        SupplierCategory,
        Attribute1_Colour,
        Attribute2_Size,
        Attribute3_PrintMethod,
        Attribute4_PrintPositon,
        Attribute5_PrintArea,
        Attribute6_PrintColours,
        Attribute7_MOQ,
        Attribute8_Material,
        Attribute9_AdditionalCharges,
        Attribute10_LeadTime,
        CofO,
        SupplierPriceExpiry,

        PrintOptionDescription,
        Price50,
        Price100,
        Price250,
        Price500,
        Price1000,
        Price2500,
        Price5000,

        OneColourPadPrint,
        OneColourScreenPrint,
        OneColourTransferPrint,
        EmbossingOrOneColourHotStamp,
        LaserEngraving,
        FullColourPrint,
        FullColourSublimation,
    }
}
