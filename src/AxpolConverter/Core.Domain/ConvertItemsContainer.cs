﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AxpolConverter
{
    public class ConvertItemsContainer
    {
        public ConvertItemsContainer()
        {
            InvalidItems = new List<RowData>();
            ValidItems = new List<AxpolItem>();
        }
        public List<RowData> InvalidItems { get; set; }
        public List<AxpolItem> ValidItems { get; set; }
        public string FileName { get; set; }
    }
}
