﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AxpolConverter
{
    [Serializable]
    [XmlType("Row")]
    public class RowPrint
    {
        public RowPrint()
        {
            Errors = new List<string>();
        }
        public string CodeERP { get; set; }
        public string Position_1_PrintPosition { get; set; }
        public string Position_1_PrintSize { get; set; }
        public string Position_1_PrintTech_1 { get; set; }
        public string Position_1_PrintTech_2 { get; set; }
        public string Position_1_PrintTech_3 { get; set; }
        public string Position_1_PrintTech_4 { get; set; }
        public string Position_1_PrintTech_5 { get; set; }
        public string Position_2_PrintPosition { get; set; }
        public string Position_2_PrintSize { get; set; }
        public string Position_2_PrintTech_1 { get; set; }
        public string Position_2_PrintTech_2 { get; set; }
        public string Position_2_PrintTech_3 { get; set; }
        public string Position_2_PrintTech_4 { get; set; }
        public string Position_2_PrintTech_5 { get; set; }
        public string Position_3_PrintPosition { get; set; }
        public string Position_3_PrintSize { get; set; }
        public string Position_3_PrintTech_1 { get; set; }
        public string Position_3_PrintTech_2 { get; set; }
        public string Position_3_PrintTech_3 { get; set; }
        public string Position_3_PrintTech_4 { get; set; }
        public string Position_3_PrintTech_5 { get; set; }
        public string Position_4_PrintPosition { get; set; }
        public string Position_4_PrintSize { get; set; }
        public string Position_4_PrintTech_1 { get; set; }
        public string Position_4_PrintTech_2 { get; set; }
        public string Position_4_PrintTech_3 { get; set; }
        public string Position_4_PrintTech_4 { get; set; }
        public string Position_4_PrintTech_5 { get; set; }
        public string Position_5_PrintPosition { get; set; }
        public string Position_5_PrintSize { get; set; }
        public string Position_5_PrintTech_1 { get; set; }
        public string Position_5_PrintTech_2 { get; set; }
        public string Position_5_PrintTech_3 { get; set; }
        public string Position_5_PrintTech_4 { get; set; }
        public string Position_5_PrintTech_5 { get; set; }
        public string Position_6_PrintPosition { get; set; }
        public string Position_6_PrintSize { get; set; }
        public string Position_6_PrintTech_1 { get; set; }
        public string Position_6_PrintTech_2 { get; set; }
        public string Position_6_PrintTech_3 { get; set; }
        public string Position_6_PrintTech_4 { get; set; }
        public string Position_6_PrintTech_5 { get; set; }
        [XmlIgnore]
        public List<string> Errors { get; set; }
    }
}
