﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AxpolConverter
{
    public class SettingsDto
    {
        public string Supplier { get; set; }

        public string SupplierCode { get; set; }

        public string MOQ { get; set; }

        public string AdditionalCharges { get; set; }

        public string ConvertionRate { get; set; }

        public string AxplDiscount { get; set; }


    }
}
