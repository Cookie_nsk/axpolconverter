﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AxpolConverter
{
    public class TemplateColumnReference
    {
        public TemplateColumnReference()
        {
            ChildColumns = new List<TemplateColumnReference>();
        }
        /// <summary>
        /// Price option description, Price 50, Price 100 e.t.c.
        /// </summary>
        public XslxColumNames ColumnType { get; set; }
        /// <summary>
        /// BB,BC,BD e.t.c
        /// </summary>
        public string ColumnReference { get; set; }
        public List<TemplateColumnReference> ChildColumns { get; set; }
    }
}
