﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AxpolConverter
{
    [Serializable]
    [XmlType("Row")]
    public class RowData
    {
        public RowData()
        {
            Errors = new List<string>();
        }
        public string CodeERP { get; set; }
        public string NetPriceEUR { get; set; }
        public string CatalogPriceEUR { get; set; }
        public string NetPriceGBP { get; set; }
        public string CatalogPriceGBP { get; set; }
        public string Catalog { get; set; }
        public string MainCategoryPL { get; set; }
        public string SubCategoryPL { get; set; }
        public string MainCategoryEN { get; set; }
        public string SubCategoryEN { get; set; }
        public string MainCategoryDE { get; set; }
        public string SubCategoryDE { get; set; }
        public string MainCategoryFR { get; set; }
        public string SubCategoryFR { get; set; }
        public string Promotion { get; set; }
        public string PromotionEnd { get; set; }
        public string Recommend { get; set; }
        [XmlElement("Recommend-pl")]
        public string RecommendPl { get; set; }
        [XmlElement("Recommend-eu")]
        public string RecommendEu { get; set; }
        public string New { get; set; }
        public string Sale { get; set; }
        public string TitlePL { get; set; }
        public string TitleEN { get; set; }
        public string TitleDE { get; set; }
        public string TitleFR { get; set; }
        public string DescriptionPL { get; set; }
        public string DescriptionEN { get; set; }
        public string DescriptionDE { get; set; }
        public string DescriptionFR { get; set; }
        public string Page { get; set; }
        public string MaterialPL { get; set; }
        public string MaterialEN { get; set; }
        public string MaterialDE { get; set; }
        public string MaterialFR { get; set; }
        public string Dimensions { get; set; }
        public string CountryOfOrigin { get; set; }
        public string CustomCode { get; set; }
        public string ItemWeightG { get; set; }
        public string InnerCtnQty { get; set; }
        public string ExportCtnQty { get; set; }
        public string CtnDimensions { get; set; }
        public string CtnWeightKG { get; set; }
        public string IndividualPacking { get; set; }
        public string InkColor { get; set; }
        public string BattIncl { get; set; }
        public string BattQty { get; set; }
        public string BattType { get; set; }
        public string Film { get; set; }
        public string ColorPL { get; set; }
        public string ColorEN { get; set; }
        public string ColorDE { get; set; }
        public string ColorFR { get; set; }
        public string ExtraTextPL { get; set; }
        public string ExtraTextEN { get; set; }
        public string ExtraTextDE { get; set; }
        public string ExtraTextFR { get; set; }
        public string Video360 { get; set; }
        public string Days { get; set; }
        public string HandlingCost { get; set; }
        public string Foto01 { get; set; }
        public string Foto02 { get; set; }
        public string Foto03 { get; set; }
        public string Foto04 { get; set; }
        public string Foto05 { get; set; }
        public string Foto06 { get; set; }
        public string Foto07 { get; set; }
        public string Foto08 { get; set; }
        public string Foto09 { get; set; }
        public string Foto10 { get; set; }
        public string Foto11 { get; set; }
        public string Foto12 { get; set; }
        public string Foto13 { get; set; }
        public string Foto14 { get; set; }
        public string Foto15 { get; set; }
        public string Foto16 { get; set; }
        public string Foto17 { get; set; }
        public string Foto18 { get; set; }
        public string Foto19 { get; set; }
        public string Foto20 { get; set; }
        public List<string> Errors { get; set; }

        [XmlIgnore]
        public RowPrint PrintData { get; set; }
    }
}
