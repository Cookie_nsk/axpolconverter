﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxpolConverter.Core.Domain
{
    public class PricingCalculationParams
    {
        public double GBPConvertionRate { get; set; }
        public int Discount { get; set; }
        public int ItemsQuantity { get; set; }
        public int NumberOfColors { get; set; }
        public int PrintMethodPerItemCost { get; set; }
        public int HandlingCost { get; set; }
        public int PreparationCost { get; set; }
        public double BasicItemCost { get; set; }
        public bool WasError { get; set; }
        public double GetPriceInGBPCurrency()
        {
            return BasicItemCost * GBPConvertionRate;
        }

    }
}
