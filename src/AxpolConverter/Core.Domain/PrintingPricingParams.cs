﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxpolConverter.Core.Domain
{
    public class PrintingPricingParams
    {
        public string PrintingOptionDescription { get; set; }
        public string PrintingMethod { get; set; }
    }
}
