﻿using AxpolConverter.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AxpolConverter
{
    public class AxpolItem
    {
        /// <summary>
        /// Assign each Axpol product a SKU number from ITB1000
        /// </summary>
        public string SKUNumber { get; set; }
        /// <summary>
        /// Our column AS, their column C
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Our column AT, nothing comparable in their sheet, but if there is in the XML please use it, essentially it’s a longer description than the title
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Should simply say Axpol in all cells in column AU
        /// </summary>
        public string Supplier { get; set; }
        /// <summary>
        /// Should simply say AXP in all cells in column AV
        /// </summary>
        public string SupplierCode { get; set; }
        /// <summary>
        /// Our column AW, their column A
        /// </summary>
        public string SupplierReference { get; set; }
        /// <summary>
        /// This should be the full filename of the corresponding image (for example “mirror.jpg”). 
        /// Products with multiple images should be in the same field, separated by a comma, for example “mirror1.jpg,mirror2.jpg”
        /// </summary>
        public string Images { get; set; }
        /// <summary>
        /// Our column AY – ignore
        /// </summary>
        public string SupplierCategory { get; set; }
        /// <summary>
        /// Attribute 1 - Colour(s) - Our column AZ, no column on sheet, but this is shown on the site, so must be in the XML
        /// </summary>
        public string Colour { get; set; }
        /// <summary>
        /// Attribute 2 - Size(s) - Our column BA, no column on sheet, but this is shown on the site, so must be in the XML
        /// </summary>
        public string Size { get; set; }
        /// <summary>
        /// Attribute 3 - Print Method - Our column BB. This field needs to reflect all stated printing methods from Axpol’s data. 
        /// For example, if a product states pricing for both screen print and heat transfer, this field should read “Screen Print, Heat Transfer”.
        /// </summary>
        public string PrintMethod { get; set; }
        public string PrintPositon { get; set; }
        /// <summary>
        /// Attribute 5 - Print Area - Our column BD. Using the above again as an example, the print area is “40mm x 40mm”
        /// </summary>
        public string PrintArea { get; set; }
        /// <summary>
        /// Attribute 6 - Print Colours - Our column BE. Ignore this field.
        /// </summary>
        public string PrintColours { get; set; }
        /// <summary>
        /// Attribute 7 - MOQ - Our column BF. Use a set qty of 50 for all items.
        /// </summary>
        public string MOQ { get; set; }
        /// <summary>
        /// Attribute 8 - Material - Our column BG, their column I
        /// </summary>
        public string Material { get; set; }
        /// <summary>
        /// Attribute 9 - Additional Charges - Our column BH. Ignore this. We will likely add generic text to cover all products here, we will advise.
        /// </summary>
        public string AdditionalCharges { get; set; }
        /// <summary>
        /// Attribute 10 - Lead Time - Our column BI. Ignore.
        /// </summary>
        public string LeadTime { get; set; }
        /// <summary>
        /// CofO – Our column BJ, their column M
        /// </summary>
        public string CofO { get; set; }
        /// <summary>
        /// Supplier Price Expiry. Ignore for now.
        /// </summary>
        public string SupplierPriceExpiry { get; set; }

        [XmlIgnore]
        public RowPrint PrintData { get; set; }
        [XmlIgnore]
        public RowData RowData { get; set; }
        [XmlIgnore]
        public List<string> PrintMethods { get; set; }
        [XmlIgnore]
        public string CurrentPrintMethod { get; set; }
        /// <summary>
        /// Attribute 4 - Print Positon - Our column BC. This should state where the item is printed, which isn’t on their sheet, but is on their site so must be in the XML. 
        /// For example, see product https://axpol.com.pl/en/1368-textiles/v7171-04.html, for this product, look at the “printing” section, the print position is “Item Front”
        /// </summary>
        [XmlIgnore]
        public List<PrintingPricingParams> PrintingPricingParams { get; set; }

    }
}
