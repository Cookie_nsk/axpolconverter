﻿namespace AxpolConverter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ofd_openXmlDataFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.tbx_inputXmlFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_chooseXmlFile = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_exportToXLSX = new System.Windows.Forms.Button();
            this.tbx_xmlPrintData = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_chooseXmlPrintDataFile = new System.Windows.Forms.Button();
            this.tbx_axpolDiscountValue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbx_convertionRate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbx_additionalCharges = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbx_moq = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbx_supplierCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbx_supplier = new System.Windows.Forms.TextBox();
            this.btn_startConversion = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbx_outputFilename = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_clearConsole = new System.Windows.Forms.LinkLabel();
            this.label11 = new System.Windows.Forms.Label();
            this.pb_exportProgress = new System.Windows.Forms.ProgressBar();
            this.tbx_outConsole = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ofd_openXmlPrintDataFile = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ofd_openXmlDataFileDlg
            // 
            this.ofd_openXmlDataFileDlg.Filter = "XML files|*.xml";
            this.ofd_openXmlDataFileDlg.FileOk += new System.ComponentModel.CancelEventHandler(this.ofd_openXmlFileDlg_FileOk);
            // 
            // tbx_inputXmlFileName
            // 
            this.tbx_inputXmlFileName.BackColor = System.Drawing.Color.White;
            this.tbx_inputXmlFileName.Enabled = false;
            this.tbx_inputXmlFileName.Location = new System.Drawing.Point(3, 21);
            this.tbx_inputXmlFileName.Name = "tbx_inputXmlFileName";
            this.tbx_inputXmlFileName.Size = new System.Drawing.Size(223, 20);
            this.tbx_inputXmlFileName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Input XML data file:";
            // 
            // btn_chooseXmlFile
            // 
            this.btn_chooseXmlFile.Location = new System.Drawing.Point(232, 21);
            this.btn_chooseXmlFile.Name = "btn_chooseXmlFile";
            this.btn_chooseXmlFile.Size = new System.Drawing.Size(82, 21);
            this.btn_chooseXmlFile.TabIndex = 1;
            this.btn_chooseXmlFile.Text = "Choose file...";
            this.btn_chooseXmlFile.UseVisualStyleBackColor = true;
            this.btn_chooseXmlFile.Click += new System.EventHandler(this.btn_chooseXmlFile_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_exportToXLSX);
            this.panel1.Controls.Add(this.tbx_xmlPrintData);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.btn_chooseXmlPrintDataFile);
            this.panel1.Controls.Add(this.tbx_axpolDiscountValue);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.tbx_convertionRate);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.tbx_additionalCharges);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.tbx_moq);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.tbx_supplierCode);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tbx_supplier);
            this.panel1.Controls.Add(this.btn_startConversion);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.tbx_outputFilename);
            this.panel1.Controls.Add(this.tbx_inputXmlFileName);
            this.panel1.Controls.Add(this.btn_chooseXmlFile);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(326, 418);
            this.panel1.TabIndex = 3;
            // 
            // btn_exportToXLSX
            // 
            this.btn_exportToXLSX.Enabled = false;
            this.btn_exportToXLSX.Location = new System.Drawing.Point(159, 361);
            this.btn_exportToXLSX.Name = "btn_exportToXLSX";
            this.btn_exportToXLSX.Size = new System.Drawing.Size(155, 53);
            this.btn_exportToXLSX.TabIndex = 21;
            this.btn_exportToXLSX.Text = "Export to XLSX";
            this.btn_exportToXLSX.UseVisualStyleBackColor = true;
            this.btn_exportToXLSX.Click += new System.EventHandler(this.btn_exportToXLSX_Click);
            // 
            // tbx_xmlPrintData
            // 
            this.tbx_xmlPrintData.BackColor = System.Drawing.Color.White;
            this.tbx_xmlPrintData.Enabled = false;
            this.tbx_xmlPrintData.Location = new System.Drawing.Point(3, 60);
            this.tbx_xmlPrintData.Name = "tbx_xmlPrintData";
            this.tbx_xmlPrintData.Size = new System.Drawing.Size(223, 20);
            this.tbx_xmlPrintData.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 319);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(150, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Axpol Trade discount value %:";
            // 
            // btn_chooseXmlPrintDataFile
            // 
            this.btn_chooseXmlPrintDataFile.Location = new System.Drawing.Point(232, 60);
            this.btn_chooseXmlPrintDataFile.Name = "btn_chooseXmlPrintDataFile";
            this.btn_chooseXmlPrintDataFile.Size = new System.Drawing.Size(82, 21);
            this.btn_chooseXmlPrintDataFile.TabIndex = 19;
            this.btn_chooseXmlPrintDataFile.Text = "Choose file...";
            this.btn_chooseXmlPrintDataFile.UseVisualStyleBackColor = true;
            this.btn_chooseXmlPrintDataFile.Click += new System.EventHandler(this.btn_chooseXmlPrintDataFile_Click);
            // 
            // tbx_axpolDiscountValue
            // 
            this.tbx_axpolDiscountValue.Location = new System.Drawing.Point(3, 335);
            this.tbx_axpolDiscountValue.Name = "tbx_axpolDiscountValue";
            this.tbx_axpolDiscountValue.Size = new System.Drawing.Size(311, 20);
            this.tbx_axpolDiscountValue.TabIndex = 8;
            this.tbx_axpolDiscountValue.TextChanged += new System.EventHandler(this.IsNumber);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Input XML print data file:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 280);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Euro conversion rate:";
            // 
            // tbx_convertionRate
            // 
            this.tbx_convertionRate.Location = new System.Drawing.Point(3, 296);
            this.tbx_convertionRate.Name = "tbx_convertionRate";
            this.tbx_convertionRate.Size = new System.Drawing.Size(311, 20);
            this.tbx_convertionRate.TabIndex = 7;
            this.tbx_convertionRate.TextChanged += new System.EventHandler(this.IsNumber);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 241);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Additional Charges:";
            // 
            // tbx_additionalCharges
            // 
            this.tbx_additionalCharges.Location = new System.Drawing.Point(3, 257);
            this.tbx_additionalCharges.Name = "tbx_additionalCharges";
            this.tbx_additionalCharges.Size = new System.Drawing.Size(311, 20);
            this.tbx_additionalCharges.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 202);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "MOQ:";
            // 
            // tbx_moq
            // 
            this.tbx_moq.Location = new System.Drawing.Point(3, 218);
            this.tbx_moq.Name = "tbx_moq";
            this.tbx_moq.Size = new System.Drawing.Size(311, 20);
            this.tbx_moq.TabIndex = 5;
            this.tbx_moq.TextChanged += new System.EventHandler(this.IsNumber);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Supplier code:";
            // 
            // tbx_supplierCode
            // 
            this.tbx_supplierCode.Location = new System.Drawing.Point(3, 179);
            this.tbx_supplierCode.Name = "tbx_supplierCode";
            this.tbx_supplierCode.Size = new System.Drawing.Size(311, 20);
            this.tbx_supplierCode.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Supplier:";
            // 
            // tbx_supplier
            // 
            this.tbx_supplier.Location = new System.Drawing.Point(3, 140);
            this.tbx_supplier.Name = "tbx_supplier";
            this.tbx_supplier.Size = new System.Drawing.Size(311, 20);
            this.tbx_supplier.TabIndex = 3;
            // 
            // btn_startConversion
            // 
            this.btn_startConversion.Location = new System.Drawing.Point(3, 361);
            this.btn_startConversion.Name = "btn_startConversion";
            this.btn_startConversion.Size = new System.Drawing.Size(155, 53);
            this.btn_startConversion.TabIndex = 9;
            this.btn_startConversion.Text = "Start conversion";
            this.btn_startConversion.UseVisualStyleBackColor = true;
            this.btn_startConversion.Click += new System.EventHandler(this.btn_startConversion_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Output filename:";
            // 
            // tbx_outputFilename
            // 
            this.tbx_outputFilename.Location = new System.Drawing.Point(3, 101);
            this.tbx_outputFilename.Name = "tbx_outputFilename";
            this.tbx_outputFilename.Size = new System.Drawing.Size(311, 20);
            this.tbx_outputFilename.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lbl_clearConsole);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.pb_exportProgress);
            this.panel2.Controls.Add(this.tbx_outConsole);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(344, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(376, 418);
            this.panel2.TabIndex = 4;
            // 
            // lbl_clearConsole
            // 
            this.lbl_clearConsole.AutoSize = true;
            this.lbl_clearConsole.Location = new System.Drawing.Point(302, 5);
            this.lbl_clearConsole.Name = "lbl_clearConsole";
            this.lbl_clearConsole.Size = new System.Drawing.Size(71, 13);
            this.lbl_clearConsole.TabIndex = 23;
            this.lbl_clearConsole.TabStop = true;
            this.lbl_clearConsole.Text = "Clear console";
            this.lbl_clearConsole.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbl_clearConsole_LinkClicked);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 372);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Operation progress";
            // 
            // pb_exportProgress
            // 
            this.pb_exportProgress.Location = new System.Drawing.Point(6, 388);
            this.pb_exportProgress.Minimum = 1;
            this.pb_exportProgress.Name = "pb_exportProgress";
            this.pb_exportProgress.Size = new System.Drawing.Size(367, 23);
            this.pb_exportProgress.Step = 1;
            this.pb_exportProgress.TabIndex = 5;
            this.pb_exportProgress.Value = 1;
            // 
            // tbx_outConsole
            // 
            this.tbx_outConsole.BackColor = System.Drawing.SystemColors.Window;
            this.tbx_outConsole.Location = new System.Drawing.Point(4, 21);
            this.tbx_outConsole.Name = "tbx_outConsole";
            this.tbx_outConsole.ReadOnly = true;
            this.tbx_outConsole.Size = new System.Drawing.Size(369, 348);
            this.tbx_outConsole.TabIndex = 4;
            this.tbx_outConsole.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Output console:";
            // 
            // ofd_openXmlPrintDataFile
            // 
            this.ofd_openXmlPrintDataFile.Filter = "XML files|*.xml";
            this.ofd_openXmlPrintDataFile.FileOk += new System.ComponentModel.CancelEventHandler(this.ofd_openXmlPrintDataFile_FileOk);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 435);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Axpol converter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog ofd_openXmlDataFileDlg;
        private System.Windows.Forms.TextBox tbx_inputXmlFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_chooseXmlFile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbx_outputFilename;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox tbx_outConsole;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_startConversion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbx_supplier;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbx_supplierCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbx_moq;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbx_additionalCharges;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbx_axpolDiscountValue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbx_convertionRate;
        private System.Windows.Forms.TextBox tbx_xmlPrintData;
        private System.Windows.Forms.Button btn_chooseXmlPrintDataFile;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.OpenFileDialog ofd_openXmlPrintDataFile;
        private System.Windows.Forms.Button btn_exportToXLSX;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ProgressBar pb_exportProgress;
        private System.Windows.Forms.LinkLabel lbl_clearConsole;
    }
}

