﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Xml.Serialization;
using System.Threading;

namespace AxpolConverter
{
    public partial class MainForm : Form
    {
        #region common
        private string _delimeter = "      ------------------------------------------------------------------------------------------------------------      \r\n";

        Dictionary<string, string> _templatePricesColumnNamesArray = new Dictionary<string, string>()
            {
                { "G", "T1" }, { "H", "T2" },{ "I", "T3" },{ "J", "T4" },{ "K", "S1" },{ "L", "S2" },{ "M", "S3" },{ "N", "S4" },{ "O", "TF1" },{ "P", "TF2" },
                { "Q", "TF3" },{ "R", "TF4" },{ "S", "KD7" },{ "T", "TC1" },{ "U", "TC2" },{ "V", "H" },{ "W", "D" },{ "X", "L0" },{ "Y", "L1" },{ "Z", "L2" },
                { "AA", "L3" },{ "AB", "L4" },{ "AC", "FC1" },{ "AD", "FC2" },{ "AE", "SL" }
            };
        #endregion

        private BackgroundWorker ConvertionWorker { get; set; }
        private BackgroundWorker ExportWorker { get; set; }
        private ConvertItemsContainer СonvertItems { get; set; }
        private Dictionary<string, double> HandlingCost { get; set; }
        private Dictionary<string, double> PreparationCost { get; set; }
        private Dictionary<int, Dictionary<string, double>> PrintMethodCost { get; set; }
        private List<TemplateColumnReference> PricesColumnReferences { get; set; }
        private List<TemplateColumnReference> DataColumnReferences { get; set; }
        private bool PricesSettingsCorrupted { get; set; }
        
        #region Main form methods
        public MainForm()
        {
            InitializeComponent();
            RestoreConfigurationSettings();

            tbx_outputFilename.Text = string.Format("ITB-Product-List-{0}.xlsx", DateTime.Now.ToString("ddMMyyyyHHmm"));

            ConvertionWorker = new BackgroundWorker();
            ConvertionWorker.DoWork += new DoWorkEventHandler(convertionWorker_doWork);
            ConvertionWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(convertionWorker_workCompleted);

            ExportWorker = new BackgroundWorker();
            ExportWorker.DoWork += new DoWorkEventHandler(exportWorker_DoWork);
            ExportWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(exportWorker_RunWorkerCompleted);

            LoadHandlingCostDictionary();
            LoadPreparationCostDictionary();
            LoadPrintMethodCostDictionary();
            LoadColumnPricesDefinitions();
            LoadColumnDataDefinitions();

            if (PricesSettingsCorrupted)
            {
                SetDisabledMainFormState();
            }
            if ((PricesColumnReferences == null || PricesColumnReferences.Count == 0 || 
                PricesColumnReferences.SelectMany(x => x.ChildColumns).Count() < 42))
            {//42- number of item prices 50,100... (7 in total) * by the number of options - 6. See Template.xslx -> Printing price list -> column references
                MessageBox.Show("There was an error parsing settings for Column references. Please correct template, or reinstall application.");
                SetDisabledMainFormState();
            }

            if ((DataColumnReferences == null || DataColumnReferences.Count == 0 ||
                PricesColumnReferences.SelectMany(x => x.ChildColumns).Count() < 21))
            {//21- number of data columns. See Template.xslx -> Printing price list -> Data column references
                MessageBox.Show("There was an error parsing settings for Data column references. Please correct template, or reinstall application.");
                SetDisabledMainFormState();
            }

        }
        private void btn_chooseXmlFile_Click(object sender, EventArgs e)
        {
            ofd_openXmlDataFileDlg.ShowDialog(this);
        }
        private void btn_chooseXmlPrintDataFile_Click(object sender, EventArgs e)
        {
            ofd_openXmlPrintDataFile.ShowDialog(this);
        }
        private void btn_startConversion_Click(object sender, EventArgs e)
        {
            if (CheckIfConvertionPossible())
            {
                ThreadParams threadParams = new ThreadParams()
                {
                    MainDataFileName = tbx_inputXmlFileName.Text,
                    PrintDataFileName = tbx_xmlPrintData.Text,
                    SettingsData = GetSettingsData(),
                    ProgressBar = pb_exportProgress,
                    MainForm = this,
                };

                btn_startConversion.Enabled = false;

                SetConsoleOutputMessage("Processing data, please wait....\r\n");

                ConvertionWorker.RunWorkerAsync(threadParams);
            }
            else
            {
                MessageBox.Show("Please upload valid XML for main data and print data");
            }
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveConfigurationSettings();
        }
        private void ofd_openXmlFileDlg_FileOk(object sender, CancelEventArgs e)
        {
            ThreadParams parameters = new ThreadParams()
            {
                Control = tbx_inputXmlFileName,
                FileName = (sender as OpenFileDialog).FileName,
                SchemaFileName = "RowDataXsdSchema.xsd",
                LoadDataType = "main data",
            };

            SetConsoleOutputMessage(string.Format("Xml data file {0} loaded.\r\n", (sender as OpenFileDialog).SafeFileName));
            new Thread(new ParameterizedThreadStart(LoadXmlFile)).Start(parameters);
        }
        private void ofd_openXmlPrintDataFile_FileOk(object sender, CancelEventArgs e)
        {
            ThreadParams parameters = new ThreadParams()
            {
                Control = tbx_xmlPrintData,
                FileName = (sender as OpenFileDialog).FileName,
                SchemaFileName = "RowPrintDataXsdSchema.xsd",
                LoadDataType = "print data",
            };

            SetConsoleOutputMessage(string.Format("Print data file {0} loaded.\r\n", (sender as OpenFileDialog).SafeFileName));
            new Thread(new ParameterizedThreadStart(LoadXmlFile)).Start(parameters);
        }
        private void btn_exportToXLSX_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbx_outputFilename.Text))
            {
                MessageBox.Show("Enter output file name.");
                return;
            }

            string fullXslxFilePath = string.Format("./ExportResults/{0}", tbx_outputFilename.Text);

            if (CopyXslxFileTemplateToNewFolder(fullXslxFilePath))
            {
                SetConsoleOutputMessage("Export to XSLX started.\r\n");

                ThreadParams threadParams = new ThreadParams()
                {
                    OutputFileName = fullXslxFilePath,
                    ExportData = СonvertItems,
                    StartedAt = DateTime.Now,
                    ProgressBar = pb_exportProgress,
                    SettingsData = GetSettingsData(),
                    HandlingCost = this.HandlingCost,
                    PreparationCost = this.PreparationCost,
                    PrintMethodCost = this.PrintMethodCost,
                    PricesColumnReferences = this.PricesColumnReferences,
                    DataColumnReferences = this.DataColumnReferences,
                };

                SetDisabledMainFormState();

                ExportWorker.RunWorkerAsync(threadParams);
            }
        }
        private void lbl_clearConsole_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tbx_outConsole.Text = "";
        }
        private void LoadXmlFile(object prms)
        {
            ThreadParams threadParams = (ThreadParams)prms;

            SetConsoleOutputMessage(string.Format("Validating {0} XML by XSD schema...\r\n", threadParams.LoadDataType));

            List<string> xsdValidationErrors =
                XmlHelper.ValidateXmlByXsdSchema(threadParams.FileName, threadParams.SchemaFileName);

            if (xsdValidationErrors.Count > 0)
            {
                SetConsoleOutputMessage(string.Format("Validating {0} XML by XSD failed. Probably wrong XML.\r\n", threadParams.LoadDataType));
                xsdValidationErrors.Distinct().ToList().ForEach(x => SetConsoleOutputMessage(string.Format("{0}\r\n", x)));
            }
            else
            {
                SetConsoleOutputMessage(string.Format("XML {0} Validation SUCCESS.\r\n", threadParams.LoadDataType));
                SetTextBoxValues(threadParams.Control as TextBox, threadParams.FileName);
            }

            SetConsoleOutputMessage(_delimeter);
        }
        private bool CheckIfConvertionPossible()
        {
            return !string.IsNullOrEmpty(tbx_xmlPrintData.Text) && !string.IsNullOrEmpty(tbx_xmlPrintData.Text);
        }
        private void IsNumber(object sender, EventArgs e)
        {
            double res = 0;
            if (!double.TryParse((sender as TextBox).Text, NumberStyles.AllowDecimalPoint, CultureInfo.CurrentUICulture, out res))
            {
                (sender as TextBox).Text = string.Empty;
            }
        }
        private void SetUpProgressBar(int totalValues)
        {
            pb_exportProgress.Minimum = 1;
            pb_exportProgress.Maximum = totalValues;
            pb_exportProgress.Value = 1;
            pb_exportProgress.Step = 1;
        }
        private void SetDisabledMainFormState()
        {
            tbx_inputXmlFileName.Text = string.Empty;
            tbx_xmlPrintData.Text = string.Empty;
            btn_exportToXLSX.Enabled = false;
            btn_startConversion.Enabled = false;
            btn_chooseXmlFile.Enabled = false;
            btn_chooseXmlPrintDataFile.Enabled = false;
            lbl_clearConsole.Enabled = false;
        }
        private void SetInitMainFormState()
        {
            btn_chooseXmlFile.Enabled = true;
            btn_chooseXmlPrintDataFile.Enabled = true;
            btn_startConversion.Enabled = true;
            lbl_clearConsole.Enabled = true;
        }
        public static bool CopyXslxFileTemplateToNewFolder(string fileName)
        {
            if (!Directory.Exists("./ExportResults"))
            {
                Directory.CreateDirectory("./ExportResults");
            }
            try
            {
                if (File.Exists(fileName))
                {
                    if (MessageBox.Show("File with same name exists in directory, overwrite?", "File exists", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        return false;
                    }
                }
                File.Copy("./Xlsx/Template.xlsx", fileName, true);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.InnerException.ToString());
                NLogHelper.Error(exp);
                return false;
            }
            return true;
        }

        #region Threading
        private void convertionWorker_doWork(object sender, DoWorkEventArgs e)
        {
            ThreadParams threadParams = (ThreadParams)e.Argument;
            e.Result = XmlHelper.CreateItemsForConvertion(threadParams);
        }
        private void convertionWorker_workCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            СonvertItems = (ConvertItemsContainer)e.Result;

            SetConsoleOutputMessage("Processing complete.\r\n");
            SetConsoleOutputMessage(string.Format("Total items: {0}.\r\n", СonvertItems.InvalidItems.Count + СonvertItems.ValidItems.Count));
            SetConsoleOutputMessage(string.Format("Valid items: {0}.\r\n", СonvertItems.ValidItems.Count));
            SetConsoleOutputMessage(string.Format("Invalid items: {0}.\r\n", СonvertItems.InvalidItems.Count));

            if (СonvertItems.ValidItems.Count > 0)
            {
                btn_startConversion.Enabled = true;
                btn_exportToXLSX.Enabled = true;
                SetUpProgressBar(СonvertItems.ValidItems.Count);
            }

            SetConsoleOutputMessage(_delimeter);
            SetConsoleOutputMessage(string.Format("Saving invalid items to ExportResults.\r\n"));

            SaveInvalidItems(СonvertItems);
            string fileName = string.Format(@"{0}\{1}", Application.StartupPath, СonvertItems.FileName);

            SetConsoleOutputMessage(string.Format("Invalid items file created in folder: {0}.\r\n", fileName));
            SetConsoleOutputMessage(_delimeter);
        }
        private void exportWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ThreadParams threadParams = (ThreadParams)e.Argument;
            e.Result = XlsxHelper.CreateXlsxFileEntries(threadParams);
        }
        private void exportWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ThreadParams parameters = (ThreadParams)e.Result;

            if (parameters.Exception != null)
            {
                SetConsoleOutputMessage("Export failed with errors:\r\n");
                SetConsoleOutputMessage(string.Format("{0}:\r\n", parameters.Exception.ToString()));
                SetConsoleOutputMessage(_delimeter);
            }
            else
            {
                string fileName = string.Format(@"{0}\{1}", Application.StartupPath,
                    parameters.OutputFileName.Substring(2, parameters.OutputFileName.Length - 2)).Replace("/", "\\");

                SetConsoleOutputMessage("Export to XSLX finished.\r\n");
                SetConsoleOutputMessage(string.Format("Elapsed time:{0}.\r\n", (DateTime.Now - parameters.StartedAt).ToString()));
                SetConsoleOutputMessage(_delimeter);
                SetConsoleOutputMessage(
                    string.Format("File created in: {0}\r\n.", fileName));
                SetConsoleOutputMessage(_delimeter);

                pb_exportProgress.Invoke((MethodInvoker)delegate
                {
                    SetUpProgressBar(2);
                });
            }


            SetInitMainFormState();
        }
        private void SaveInvalidItems(ConvertItemsContainer items)
        {
            if (!Directory.Exists("./ExportResults"))
            {
                Directory.CreateDirectory("./ExportResults");
            }
            try
            {
                DateTime now = DateTime.Now;

                while (File.Exists(string.Format("invalidItems-{0}.xlsx", now.ToString("ddMMyyyyHHmm"))))
                {
                    now = now.AddMinutes(2);
                }
                items.FileName = string.Format("invalidItems-{0}.xlsx", now.ToString("ddMMyyyyHHmm"));
                File.Copy("./Xlsx/TemplateErrors.xlsx", string.Format("./ExportResults/{0}", items.FileName), true);
            }
            catch (Exception exp)
            {
                NLogHelper.Error(exp);
            }

            XlsxHelper.CreateErrorXlsl(items);
        }
        #endregion

        #endregion

        #region Settings section
        private void RestoreConfigurationSettings()
        {
            tbx_supplier.Text = ConfigurationManager.AppSettings.Get("supplierText");
            tbx_supplierCode.Text = ConfigurationManager.AppSettings.Get("supplierCodeText");
            tbx_moq.Text = ConfigurationManager.AppSettings.Get("moq");
            tbx_additionalCharges.Text = ConfigurationManager.AppSettings.Get("additionalCharges");
            tbx_convertionRate.Text = ConfigurationManager.AppSettings.Get("convertionRate");
            tbx_axpolDiscountValue.Text = ConfigurationManager.AppSettings.Get("axplDiscount");
        }
        private void SaveConfigurationSettings()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);

            config.AppSettings.Settings["supplierText"].Value = tbx_supplier.Text;
            config.AppSettings.Settings["supplierCodeText"].Value = tbx_supplierCode.Text;
            config.AppSettings.Settings["moq"].Value = tbx_moq.Text;
            config.AppSettings.Settings["additionalCharges"].Value = tbx_additionalCharges.Text;
            config.AppSettings.Settings["convertionRate"].Value = tbx_convertionRate.Text;
            config.AppSettings.Settings["axplDiscount"].Value = tbx_axpolDiscountValue.Text;

            config.Save(ConfigurationSaveMode.Modified);
        }
        private SettingsDto GetSettingsData()
        {
            return new SettingsDto()
            {
                AdditionalCharges = tbx_additionalCharges.Text,
                AxplDiscount = tbx_axpolDiscountValue.Text,
                ConvertionRate = tbx_convertionRate.Text,
                MOQ = tbx_moq.Text,
                Supplier = tbx_supplier.Text,
                SupplierCode = tbx_supplierCode.Text,
            };
        }
        private void LoadHandlingCostDictionary()
        {
            CultureInfo cult = new CultureInfo("en-GB");
            try
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open("./xlsx/Template.xlsx", false))
                {
                    HandlingCost = new Dictionary<string, double>();

                    WorkbookPart wbPart = spreadsheetDocument.WorkbookPart;
                    Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Printing price list").FirstOrDefault();

                    if (theSheet == null)
                    {
                        throw new ArgumentException("Printing price list not found");
                    }
                    WorksheetPart wsPart =
                        (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                    for (int i = 1; i <= 6; i++)
                    {
                        Cell cell = wsPart.Worksheet.Descendants<Cell>().
                            Where(c => c.CellReference == string.Format("B{0}", i + 1)).FirstOrDefault();

                        if (cell == null)
                        {
                            throw new ArgumentException("no such cell");
                        }

                        HandlingCost.Add(string.Format("A{0}", i),
                            double.Parse(cell.InnerText, NumberStyles.Any, cult));
                    }
                }

            }
            catch (Exception exp)
            {
                PricesSettingsCorrupted = true;
                MessageBox.Show("There was an error parsing settings for handling cost. Please correct template, or reinstall application.");
                NLogHelper.Error(exp);
            }
        }
        private void LoadPreparationCostDictionary()
        {
            try
            {
                CultureInfo cult = new CultureInfo("en-GB");
                PreparationCost = new Dictionary<string, double>();

                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open("./xlsx/Template.xlsx", false))
                {
                    WorkbookPart wbPart = spreadsheetDocument.WorkbookPart;
                    Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Printing price list").FirstOrDefault();

                    if (theSheet == null)
                    {
                        throw new ArgumentException("Printing price list not found");
                    }
                    WorksheetPart wsPart =
                        (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                    foreach (KeyValuePair<string, string> item in _templatePricesColumnNamesArray)
                    {
                        Cell cell = wsPart.Worksheet.Descendants<Cell>().
                            Where(c => c.CellReference == string.Format("{0}19", item.Key)).FirstOrDefault();

                        if (cell == null)
                        {
                            throw new ArgumentException("no such cell");
                        }

                        PreparationCost.Add(item.Value,
                        double.Parse(cell.InnerText, NumberStyles.Any, cult));
                    }
                }
            }
            catch (Exception exp)
            {
                PricesSettingsCorrupted = true;
                MessageBox.Show("There was an error parsing settings for preparation cost. Please correct template, or reinstall application.");
                NLogHelper.Error(exp);
            }
        }
        private void LoadPrintMethodCostDictionary()
        {
            try
            {
                CultureInfo cult = new CultureInfo("en-GB");

                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open("./xlsx/Template.xlsx", false))
                {
                    PrintMethodCost = new Dictionary<int, Dictionary<string, double>>();

                    Dictionary<int, int> pmCostRowQuantityRelation = new Dictionary<int, int>()
                    {
                        { 50, 11},
                        { 100, 10},
                        { 250, 9},
                        { 500, 8},
                        { 1000, 7},
                        { 2500, 6},
                        { 5000, 5},
                    };

                    WorkbookPart wbPart = spreadsheetDocument.WorkbookPart;
                    Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Printing price list").FirstOrDefault();

                    if (theSheet == null)
                    {
                        throw new ArgumentException("Printing price list not found");
                    }
                    WorksheetPart wsPart =
                        (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                    foreach (KeyValuePair<int, int> rowQuanRel in pmCostRowQuantityRelation)
                    {//rowQuanRel.Value = 10,9,8 - row number
                        Dictionary<string, double> prntMethodDic = new Dictionary<string, double>();
                        foreach (KeyValuePair<string, string> item in _templatePricesColumnNamesArray)
                        {//itm.Key - column name, itm.Value - print method name T1,T2, e.t.c.
                            Cell cell = wsPart.Worksheet.Descendants<Cell>().
                            Where(c => c.CellReference == string.Format("{0}{1}", item.Key, rowQuanRel.Value)).FirstOrDefault();

                            if (cell == null)
                            {
                                throw new ArgumentException("no such cell");
                            }

                            prntMethodDic.Add(item.Value, double.Parse(cell.InnerText, NumberStyles.Any, cult));
                        }

                        PrintMethodCost.Add(rowQuanRel.Key, prntMethodDic);
                    }
                }
            }
            catch (Exception exp)
            {
                PricesSettingsCorrupted = true;
                MessageBox.Show("There was an error parsing settings for Print method cost. Please correct template, or reinstall application.");
                NLogHelper.Error(exp);
            }
        }
        private void LoadColumnPricesDefinitions()
        {
            try
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open("./xlsx/Template.xlsx", false))
                {
                    PricesColumnReferences = new List<TemplateColumnReference>();

                    WorkbookPart wbPart = spreadsheetDocument.WorkbookPart;
                    Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Printing price list").FirstOrDefault();
                    var stringTable = wbPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                    if (theSheet == null)
                    {
                        throw new ArgumentException("Printing price list not found");
                    }
                    WorksheetPart wsPart =
                        (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                    int rowIndex = 25; //Start index in template-> printing price list -> C25

                    TemplateColumnReference parentColumn = null;

                    while (true)
                    {
                        TemplateColumnReference childPricesColumn = new TemplateColumnReference();
                        Cell cell = wsPart.Worksheet.Descendants<Cell>().
                            Where(c => c.CellReference == string.Format("C{0}", rowIndex)).FirstOrDefault();

                        Cell cellVal = wsPart.Worksheet.Descendants<Cell>().
                            Where(c => c.CellReference == string.Format("D{0}", rowIndex)).FirstOrDefault();

                        if (cell == null || cellVal == null)
                        {//getting out from while, if cells values is empty, means we've got to the end of printing opts list
                            break;
                        }

                        var colName = stringTable.SharedStringTable
                            .ElementAt(int.Parse(cell.InnerText)).InnerText;

                        var colReferenceValue = stringTable.SharedStringTable
                            .ElementAt(int.Parse(cellVal.InnerText)).InnerText;

                        if (string.IsNullOrEmpty(colName) || string.IsNullOrEmpty(colReferenceValue))
                        {//getting out from while, if cells values is empty, means we've got to the end of printing opts list
                            break;
                        }

                        if (colName.Contains("PrintOptionDescription"))
                        {
                            parentColumn = new TemplateColumnReference();
                            PricesColumnReferences.Add(parentColumn);
                            parentColumn.ColumnType = XslxColumNames.PrintOptionDescription;
                            parentColumn.ColumnReference = colReferenceValue;

                            rowIndex++;
                            continue;
                        }
                        else if (colName.Equals(XslxColumNames.Price50.ToString()))
                        {
                            childPricesColumn.ColumnType = XslxColumNames.Price50;
                        }
                        else if (colName.Equals(XslxColumNames.Price100.ToString()))
                        {
                            childPricesColumn.ColumnType = XslxColumNames.Price100;
                        }
                        else if (colName.Equals(XslxColumNames.Price250.ToString()))
                        {
                            childPricesColumn.ColumnType = XslxColumNames.Price250;
                        }
                        else if (colName.Equals(XslxColumNames.Price500.ToString()))
                        {
                            childPricesColumn.ColumnType = XslxColumNames.Price500;
                        }
                        else if (colName.Equals(XslxColumNames.Price1000.ToString()))
                        {
                            childPricesColumn.ColumnType = XslxColumNames.Price1000;
                        }
                        else if (colName.Equals(XslxColumNames.Price2500.ToString()))
                        {
                            childPricesColumn.ColumnType = XslxColumNames.Price2500;
                        }
                        else if (colName.Equals(XslxColumNames.Price5000.ToString()))
                        {
                            childPricesColumn.ColumnType = XslxColumNames.Price5000;
                        }

                        childPricesColumn.ColumnReference = colReferenceValue;
                        parentColumn.ChildColumns.Add(childPricesColumn);

                        rowIndex++;
                    }
                }
            }
            catch (Exception exp)
            {
                PricesSettingsCorrupted = true;
                MessageBox.Show("There was an error parsing settings for handling cost. Please correct template, or reinstall application.");
                NLogHelper.Error(exp);
            }
        }
        private void LoadColumnDataDefinitions()
        {
            try
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open("./xlsx/Template.xlsx", false))
                {
                    DataColumnReferences = new List<TemplateColumnReference>();

                    WorkbookPart wbPart = spreadsheetDocument.WorkbookPart;
                    Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == "Printing price list").FirstOrDefault();
                    var stringTable = wbPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                    if (theSheet == null)
                    {
                        throw new ArgumentException("Printing price list not found");
                    }
                    WorksheetPart wsPart =
                        (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                    int rowIndex = 25; //Start index in template-> printing price list -> C25

                    TemplateColumnReference parentColumn = null;

                    while (true)
                    {
                        Cell cell = wsPart.Worksheet.Descendants<Cell>().
                            Where(c => c.CellReference == string.Format("F{0}", rowIndex)).FirstOrDefault();

                        Cell cellVal = wsPart.Worksheet.Descendants<Cell>().
                            Where(c => c.CellReference == string.Format("G{0}", rowIndex)).FirstOrDefault();

                        if (cell == null || cellVal == null)
                        {//getting out from while, if cells values is empty, means we've got to the end of printing opts list
                            break;
                        }

                        var colName = stringTable.SharedStringTable
                            .ElementAt(int.Parse(cell.InnerText)).InnerText;

                        var colReferenceValue = stringTable.SharedStringTable
                            .ElementAt(int.Parse(cellVal.InnerText)).InnerText;

                        if (string.IsNullOrEmpty(colName) || string.IsNullOrEmpty(colReferenceValue))
                        {//getting out from while, if cells values is empty, means we've got to the end of printing opts list
                            break;
                        }

                        parentColumn = new TemplateColumnReference();
                        parentColumn.ColumnType = (XslxColumNames)Enum.Parse(typeof(XslxColumNames), colName);
                        parentColumn.ColumnReference = colReferenceValue;

                        DataColumnReferences.Add(parentColumn);

                        rowIndex++;
                    }
                }
            }
            catch (Exception exp)
            {
                PricesSettingsCorrupted = true;
                MessageBox.Show("There was an error parsing settings for handling cost. Please correct template, or reinstall application.");
                NLogHelper.Error(exp);
            }
        }
        #endregion

        #region Console output methods
        private void ClearConsoleOutput()
        {
            SetConsoleOutputMessage(string.Empty);
        }
        /// <summary>
        /// Thread safe inplementation
        /// </summary>
        /// <param name="message"></param>
        public void SetConsoleOutputMessage(object message)
        {
            tbx_outConsole.Invoke(new Action(
                () => tbx_outConsole.Text = string.Format("{0}{1}", tbx_outConsole.Text, message.ToString())
                ));

            tbx_outConsole.Invoke((MethodInvoker)delegate
            {
                tbx_outConsole.Select(tbx_outConsole.Text.Length, 0);
                tbx_outConsole.ScrollToCaret();

            });
        }
        private static void SetTextBoxValues(TextBox tbx, string value)
        {
            tbx.Invoke(new Action(() => tbx.Text = value));
        }
        #endregion
    }
}
